export default {
  inserted: el => {
    function loadImage() {
      if (el) {
        el.addEventListener('load', () => {
          setTimeout(() => el.classList.add('loaded'), 100);
        });
        el.addEventListener('error', () => console.log('error'));
        el.src = el.dataset.url;
      }
    }

    function handleIntersect(entries, observer) {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          loadImage();
          observer.unobserve(el)
        }
      });
    }

    function createObserver() {
      const OPTIONS = {
        root: null,
        threshold: '0',
      };
      const OBSERVER = new IntersectionObserver(handleIntersect, OPTIONS);
      OBSERVER.observe(el);
    }

    if (window["IntersectionObserver"]) {
        createObserver();
    } else {
        loadImage();
    }
  }
};