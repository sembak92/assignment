export const ObjectValuesToString = (o) => {
  Object.keys(o).forEach((k) => {
    if (typeof o[k] === 'object') {
      return o[k].toString();
    }
    o[k] = `${o[k]}`;
  });
  return o;
};