import { BaseService } from './base';

export class ContactService extends BaseService {

  constructor() {
    super('contact');
  }

  post (form) {
    return this.client.post(`${this.namespace}`, form);
  }
}