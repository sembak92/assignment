const FILTERS = {
  categories: {
    all: 'all work',
    strategy: 'strategy',
    technology: 'technology',
    design: 'design',
    advertising: 'advertising',
  },
  industries: {
    all: 'all industries',
    media: 'media',
    recruitment: 'recruitment',
    education: 'education',
    health: 'health',
  }
}

export { FILTERS };