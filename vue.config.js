const CompressionPlugin = require('compression-webpack-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const glob = require('glob-all');
const path = require('path');

module.exports = {
  css: {
  	sourceMap: true,
    loaderOptions: {
      sass: {
        data: `
          @import "@/assets/sass/index.scss";
        `
      }
    }
  },
  configureWebpack: {
    plugins: [
      new PurgecssPlugin({
        paths: glob.sync([
          path.join(__dirname, './**/*.vue'),
          path.join(__dirname, './src/**/*.js')
        ]),
        whitelist: ['*-enter-active','*-leave-active', '*-enter', '*-leave-to', 'html'],
      }),
      new CompressionPlugin({
        algorithm: 'gzip'
      }),
    ],
  	resolve: {
  		alias: {
  			"@": require("path").resolve(__dirname, "src")
  		}
  	}
  }
};