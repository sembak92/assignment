import Axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import { CASES_ONE, CASES_TWO } from '@/JSON/cases';
import { FILTERS } from '@/JSON/filters';

let baseUrl = `//deptagency.com/api`;

if (process.env.NODE_ENV === 'staging') {
	baseUrl = `//staging.deptagency.com/api`;
} else if (process.env.NODE_ENV === 'development') {
  baseUrl = `//deptagency.test/api`;
}

export const Api = Axios.create({
  baseURL: baseUrl,
});

Api.interceptors.request.use((config) => {
  return config;
});

// Finish or Fail progress bar
Api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (!error.response) {
      return Promise.reject(error);
    }
    return Promise.reject(error.response.data);
  },
);

const MOCK = new MockAdapter(Api);

MOCK.onGet('/cases').reply(200, {
  data: {
    cases: {
      one: CASES_ONE,
      two: CASES_TWO,
    },
    filters: FILTERS,
  }
});

MOCK.onPost('/contact').reply(200, {
  success: true,
  message: 'We\'ve successfully received your message.'
});