import { CasesService } from '@/services/cases';
import { SET_LOADER, SET_CASES } from './mutation-types';

export const cases = {
  namespaced: true,
  state: {
    loading: false,
    cases: undefined,
    casesService: new CasesService(),
  },
  mutations: {
    [SET_LOADER] (state, payload) {
      state.loading = payload;
    },
    [SET_CASES] (state, payload) {
      state.cases = payload;
    }
  },
  actions: {
    async get ({ state, commit}) {
      commit(SET_LOADER, true);
      try {
        const { data } = await state.casesService.get();
        commit(SET_CASES, data.data);
        commit(SET_LOADER, false);
        return data;
      } catch(e) {
        commit(SET_LOADER, false);
        throw e;
      }
    }
  },
  getters: {
    loading: state => state.loading,
    cases: state => state.cases,
  }
}