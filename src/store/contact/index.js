import { ContactService } from '@/services/contact';
import { SET_LOADER } from './mutation-types';

export const contact = {
  namespaced: true,
  state: {
    loading: false,
    contactService: new ContactService(),
  },
  mutations: {
    [SET_LOADER] (state, payload) {
      state.loading = payload;
    },
  },
  actions: {
    async post ({ state, commit }, form) {
      commit(SET_LOADER, true);
      try {
        const { data } = await state.contactService.post(form);
        commit(SET_LOADER, false);
        return data;
      } catch(e) {
        commit(SET_LOADER, false);
        throw e;
      }
    }
  },
  getters: {
    loading: state => state.loading,
  }
}