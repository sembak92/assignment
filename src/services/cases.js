import { BaseService } from './base';

export class CasesService extends BaseService {

  constructor() {
    super('cases');
  }

  get () {
    return this.client.get(`${this.namespace}`);
  }
}