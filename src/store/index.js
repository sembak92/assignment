import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import { cases } from './cases';
import { contact } from './contact';

const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    cases,
    contact,
  }
})

export { store };
