# Frontend assignment
In this repo you will find the frontend assignment, instructions to view/build the assignment and the prerequisites.

## Getting started
To run the application download or clone the repository and follow the steps given in the Prerequisites section.

### Prerequisites

#### Mac users
To run the application from the /root/dist/ you can use a simple http server.
For Mac user open terminal go to the /root/dist/ folder and run the following command:
```
python -m SimpleHTTPServer 8000
```
This command will run a localhost server on port 8000. To view the application open your browser and go to localhost:8000

#### Windows users
The python command for windows users is not included in their OS.
To make use of this command on Windows follow the steps given in these instructions: [python for windows](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/set_up_a_local_testing_server#Running_a_simple_local_HTTP_server)

### Installing
To install the application you will have to install yarn on your computer. If you do not have yarn installed follow the instructions [here](https://yarnpkg.com/lang/en/docs/install/#mac-stable).

Once Yarn is installed navigate to the /root/ folder via your terminal or Command Prompt. In the root run the following command:
```
yarn
```
This command will install all node modules needed for the project. Once installed you can run the following command to serve the application:
```
yarn serve
```
Your terminal or Command Prompt will display the localhost server you can view the application on.

### Build
To build the application navigate to the root folder via your terminal or Command Prompt and rund the following command:
```
yarn build
```
This command will delete the current dist folder and create a fresh one with the latest build files.