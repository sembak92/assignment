import Home from '@/pages/Home.vue';

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '*',
    redirect: { name: 'home' }
  }
];

export { routes }